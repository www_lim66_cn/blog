module.exports = {
  productionSourceMap: false,
  lintOnSave: false, //关闭lint校验
  outputDir: 'blog.client',
  devServer: {
     host: 'localhost',
      port: 7788,
        proxy: {
            '/api': {
                target: 'http://localhost:7776', //本地服务器
                changeOrigin: true,
                pathRewrite: {
                    '^/api': '/api'
                }
          },
          '/aplayer': {
            target: 'https://www.limin.ac.cn', //本地服务器
            changeOrigin: true,
            pathRewrite: {
              '^/aplayer': '/aplayer'
            }
          },
          '/req_ip': {
            target: 'http://pv.sohu.com',
            changeOrigin: true,
            pathRewrite: {
              '^/req_ip': ''
            }
          }
        }
    },
}
